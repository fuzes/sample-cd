FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1
RUN apk update \
	&& apk add python3-dev

RUN addgroup -S test \
	&& adduser -S test -G test -h "/app"

COPY ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt && rm requirements.txt

COPY . /app
RUN chown -R test:test /app
USER test
WORKDIR /app
